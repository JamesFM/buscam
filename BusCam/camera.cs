using System;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace GeoGraphicsLab.GpsTools
{
    public class Camera
    {	//url of securicam to capture image from
        private static string url;
        private static string username;
        private static string password;
        private static string cameraType;
        //default bitmap if no connectivity
        private static Bitmap no_frame;
        private Bitmap picture;
        //private Stream picturestream;
        

        public Camera(string hostname, string user, string pass, string errorFrame, string type)
        {   //Constructer, takes the url of the securicam, and the username and password to auth
            url = String.Format("http://{0}/cgi-bin/video.jpg", hostname);
            username = user;
            password = pass;
            cameraType = type;
            no_frame = (Bitmap)Bitmap.FromFile(errorFrame);
            picture = no_frame;					//assign picture to no_frame by default
        }

        public void GetPicture()
        {   //interface for camera types
            if (cameraType == "USB") GetPictureUSB();
            if (cameraType == "NET") GetPictureNetwork();
            //else throw an exception
            else throw new System.Exception("Invalid camera type, can't get picture.");
        }

        public void Stamp(string lat_long, string time)
        {	//draws a string on a bitmap
            //create a new font
            Font fontie = new Font("Arial", 8);
            //create a graphics object 
            Graphics graphie = Graphics.FromImage(picture);
            graphie.DrawRectangle(new Pen(new SolidBrush(Color.Black), 9), new Rectangle(0, 0, 352, 9));
            graphie.DrawString(lat_long, fontie, new SolidBrush(Color.AntiqueWhite), 0, 0);
            graphie.DrawString(time, fontie, new SolidBrush(Color.AntiqueWhite), 230, 0); //240, 0);
            graphie.Flush();
        }

        public void SavePicture(string imagePath, string imageName)
        {	// Save the image as a JPEG					
            //TODO: Program should create image directory if it doesn't exist
            try { picture.Save(imagePath + imageName, System.Drawing.Imaging.ImageFormat.Jpeg); }
            catch
            { throw new System.Exception("Error saving image locally, check the path to the image file."); }
        }

        public Bitmap ReturnPicture()
        { return (picture); }
     
        public string NamePicture()
        //creates a picture name and directory path based on date and time
        { return (String.Format("{0:yyyy}_{0:MM}_{0:dd}_{0:dddd}_{0:hh}_{0:mm}_{0:ss}_{0:fff}.jpg", CurrTime)); }

        private void GetPictureNetwork()
        {	//FOR THE NETWORK CAMERA:
            //gets one picture off the webcam and store it in picture
            //create a new webrequest
            HttpWebRequest requests = (HttpWebRequest)WebRequest.Create(url);
            //create a network credentials object to log in
            NetworkCredential creds = new NetworkCredential(username, password);
            //add the network credentials to the request object
            requests.Credentials = creds;
            requests.Timeout = 5000;	//timeout in ms
            try
            {	//execute the request save the result in response 
                HttpWebResponse response = (HttpWebResponse)requests.GetResponse();
                //convert the response to a stream
                Stream picturestream = response.GetResponseStream();
                // convert picture stream to bitmap
                picture = (Bitmap)Image.FromStream(picturestream);
                picturestream.Close();
                //close the connection
                response.Close();
            }
            //if can't connect return false stream
            catch
            {
                throw new System.Exception("Error Connecting to Network camera.");
            }
        }

        private void GetPictureUSB()
        {	//FOR THE USB CAMERA: 
            //gets one picture off the webcam and store it in picture
            string usbframe = "orbiter.jpg";
            try
            {	//maybe a bit of a hack?
                //we may want to close this?
                picture = (Bitmap)Bitmap.FromFile(usbframe);
            }
            //if can't connect return false stream
            catch
            {
                throw new System.Exception("Error Connecting to USB camera.");
            }
        }
    }
}
