//Dependency for Gps.Sentence
using System;
using System.Diagnostics; 	//provides classes that allow you to interact with system processes, event logs, and performance counters.

namespace GeoGraphicsLab.GpsTools
{
    public class GpsUtilities
    {
        public static DateTime DateTimeFromGpsUtc(string DMY, string HMS)
        {
            try
            {
                return new DateTime(
                      Convert.ToInt32(DMY.Substring(4, 2)) + 2000,  //year, total hack, going to need to fix this in a thousand
                      Convert.ToInt32(DMY.Substring(2, 2)),			//month
                      Convert.ToInt32(DMY.Substring(0, 2)),			//day
                      Convert.ToInt32(HMS.Substring(0, 2)),
                      Convert.ToInt32(HMS.Substring(2, 2)),
                      Convert.ToInt32(HMS.Substring(4, 2)));
            }
            catch (ArgumentOutOfRangeException ex)
            {
                System.Console.WriteLine("ArgumentOutOfRangeException ex");
                throw ex;
            }
            catch (ArgumentException ex)
            {
                System.Console.WriteLine("ArgumentException ex");
                throw ex;
            }
            catch
            {
                throw new Exception(
                   "Unhandled exception in SerialGpsInterface.GpsUtilities.DateTimeFromGpsUtc()");
            }
        }

        public static double ConvertDMmToDd(string dmm)
        {
            double dd = 0;
            try
            {
                Int32 perPos = dmm.IndexOf('.');
                double degrees = Convert.ToDouble(dmm.Substring(0, perPos - 2));
                double minutes = Convert.ToDouble(dmm.Substring(perPos - 2, 6));
                dd = degrees + (minutes / 60);
            }
            catch (FormatException ex)
            {
                System.Console.WriteLine("FormatException ex");
                throw ex;
            }
            catch (OverflowException ex)
            {
                System.Console.WriteLine("OverflowException ex");
                throw ex;
            }
            catch
            {
                throw new Exception(
                   "Unhandled exception in SerialGpsInterface.GpsUtilities.ConvertDMmToDd()");
            }
            return dd;
        }

        public static string GrepDev(string grepDevName)
        {	 //Greps device or file for GPRMC string		
            Process Gps_device = new Process();
            //calls and manages the process which greps strings from the gps device	 
            Gps_device.StartInfo.UseShellExecute = false;
            Gps_device.StartInfo.RedirectStandardOutput = true;
            Gps_device.StartInfo.FileName = "grep";
            //using grep to make sure your getting GPRMC strings
            Gps_device.StartInfo.Arguments = " -m 1 GPRMC " + grepDevName;

            Gps_device.Start();
            string Gps_device_output = Gps_device.StandardOutput.ReadToEnd();
            Gps_device.WaitForExit();

            if (Gps_device.ExitCode != 0)
                throw new Exception("Grep Exit Code: " + Gps_device.ExitCode);

            return (Gps_device_output);
        }
    }
}
