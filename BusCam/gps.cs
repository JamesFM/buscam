/* Parses data from GPS. 
Dependent on Gps.GpsUtilities, AssetUpdater.cs */
using System;
//using System.Data.SqlTypes;

namespace GeoGraphicsLab.GpsTools
{
    public class Gps
    {
        public GpsFix position;
        private string sentence_id;     //currently, this is never used
        private string gprmc_sentence;
        private string[] gprmc_parts;
        private string checksum;
        private double latitude;
        private double longitude;
        private DateTime time;
        private double bearing;
        private double speed;
        private double variation;
        private bool validFix;

        public Gps()//String sentence)	   
        {   // Gps constructor 
            this.position = new GpsFix();

            //default (bad) values
            this.checksum = "Null";
            this.sentence_id = "Null";
            //this.time = 0;				//no need to initalize, don't no what values to put anyway
            this.latitude = 181;
            this.longitude = 181;
            this.variation = 0;
            this.validFix = false;
            //for position
            this.position.Latitude = 181;  //DBNull.Value;
            this.position.Longitude = 181;  //DBNull.Value;
            this.position.Speed = 0;
            this.position.Bearing = 0;
        }

        public string GetRaw()
        //returns the sentence 
        { return this.gprmc_sentence; }

        public int GetLength()
        //returns the parts array Length
        { return this.gprmc_parts.Length; }

        public string GetTime()
        //returns the value of time
        { return this.time.ToString(); }

        public double GetLatitude()
        //returns the value of latitude
        { return this.position.Latitude; }

        public double GetLongitude()
        //returns the value of longitude
        { return this.position.Longitude; }

        public double GetBearing()
        //returns the value of bearing
        { return this.bearing; }

        public double GetSpeed()
        //returns the value of speed
        { return this.speed; }

        public bool IsValidFix()
        //returns true if the Gprmc sentence thinks its valid  
        { return this.validFix; }

        public bool IfChecksum()
        {   //what is this doing, does it work? -- Things you don't want to read as a comment...
            try
            {
                Int32 calculatedChecksum = 0;
                //sum the checked
                for (Int32 i = 1; i < (this.gprmc_sentence.Length - 3); i++)     //(this.gprmc_sentence.Length-3); i++)     
                    calculatedChecksum ^= Convert.ToByte(this.gprmc_sentence[i]);  //^= XOR?

                //check if checksum is equal to sum of the checked
                return this.checksum == calculatedChecksum.ToString("x2").ToUpper();
            }
            catch
            { return (false); }
        }

        public void Parse()
        {	// GPRMC sentence parser	     
            if ((gprmc_parts.Length < 12) || (gprmc_parts.Length > 13))
                throw new System.Exception("Bad GPRMC Sentence");

            bool substand = false;
            int checksumPos = 12;
            //correct for substring GPRMC strings
            if (gprmc_parts.Length == 12)
            {
                checksumPos--;
                substand = true;
            }

            try
            {   //get the checksum of the sentence
                this.checksum = gprmc_parts[checksumPos].Split(new Char[] { '*' })[1];

                //get the sentence id
                this.sentence_id = gprmc_parts[0];

                //get this seconds date time stamp
                //PROBLEM: treats 06 as 0006 as opposed to 2006 - FIXED: total hack
                this.time = GpsUtilities.DateTimeFromGpsUtc(gprmc_parts[9], gprmc_parts[1]);
                //this.time.AddYears(1); 		//why is this not fucking working? probably mono

                //get latitude
                this.position.Latitude = GpsUtilities.ConvertDMmToDd(gprmc_parts[3]);
                if (gprmc_parts[4] == "S")
                    this.latitude = -this.latitude;

                //get longitude
                this.position.Longitude = GpsUtilities.ConvertDMmToDd(gprmc_parts[5]);
                if (gprmc_parts[6] == "W")
                    this.longitude = -this.longitude;

                //get speed
                this.position.Speed = Convert.ToDouble(gprmc_parts[7]);

                //get course
                this.position.Bearing = Convert.ToDouble(gprmc_parts[8]);

                //get variation
                if (substand == false)
                {
                    this.variation = Convert.ToDouble(gprmc_parts[10]);
                    if (gprmc_parts[11] == "W")
                        this.variation = -this.variation;
                }

                if (this.gprmc_parts[2] == "A")
                    this.validFix = true;
            }
            catch { throw new System.Exception("Parser Error. Bad String or No Signal."); }
        }

        public void LoadString(string grepDevName)
        {	//load GPRMC string through grep
            this.gprmc_sentence = GpsUtilities.GrepDev(grepDevName);
            //delimit sentence with coma
            this.gprmc_parts = gprmc_sentence.Split(new Char[] { ',' });
        }
    }
}
