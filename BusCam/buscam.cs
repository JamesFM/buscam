using System;
using System.Diagnostics; 	//provides classes that allow you to interact with system processes, event logs, and performance counters.
using System.Drawing;
using System.Threading;
using CommandLine.Utility;
using System.Timers;

namespace GeoGraphicsLab.GpsTools
{
    class Buscam
    {
        public static int verbosity;						//verbosity level, 0 = lowest, 4 = highest
        public static int sVerbosity;						//speech verbosity level, 0 = lowest, 5 = highest
        private static string grepDevName; 				    //device name
        private static string camAddress;					//camera ip address
        private static string camUser;						//camera user name
        private static string camPass;						//camera password
        private static string camType;
        //non-statics
        private string time;								//time for timestamp
        private string lat_long;							//lat/long for timestamp   		
        public static Gps position;
        public static Buscam bcam;
        public static BusCamOutput screen;

        public static void Main(String[] args)
        {
            //TODO: These should be moved to a constructor for buscam. Maybe?
            //Various Default Values
            //grepFileName = "./TEXT/confed_gpsfile.txt";   //"./TEXT/datefile.txt"; 			"./TEXT/bluetooth.txt"
            grepDevName = "./TEXT/GPS.txt";					//"/dev/tty.BT-GPS-3089A1-BT-GPSCOM-1"
            camAddress = "192.168.0.99"; 					// "192.168.10.145";
            camUser = "admin";
            camPass = "videodrome";
            camType = "NET";                                   //defaults to NET, USB for Usb camera
            string version = "v0.53";

            Console.WriteLine("\nBuscam {0}-- Bridgewater Geographics Lab", version);
            int valid = ArgParser(args);

            if (valid == 0)
            {
                screen = new BusCamOutput(verbosity, sVerbosity); 		//d00dz u n33d p4r4/\/\3t3rzzzz !!!1111
                bcam = new Buscam();

                //Program uses Threading- gpsthread will go and grab GPS info and save to position variable
                Thread gpsThread = new Thread(new ThreadStart(GpsInfoThread));
                gpsThread.Start();

                //Timer spawns new UploadImageThread which will Upload the image with the current value of position
                System.Timers.Timer geoTimer = new System.Timers.Timer();
                geoTimer.Elapsed += new ElapsedEventHandler(UploadImageThread);
                geoTimer.Interval = 1000;
                geoTimer.Start();

            }
            Console.WriteLine("Done! \n");
            //screen.Output("Done! \n",4,5);
        }

        public static void UploadImageThread(object source, ElapsedEventArgs e)
        {   //Thread entry for GeoTimer
            Bitmap image;

            image = bcam.GetImage();
            bcam.UploadImage(image);
        }

        public static void GpsInfoThread()
        {   //appears to be the thread entry point
            while (true)
            {
                bcam.GetGps();
            }
        }

        private void GetGps()
        {
            screen.Output("\nGetting Gps...", 2, 3);
            //temporary variable for new position information
            Gps newPosition = new Gps();
            try
            {
                newPosition.LoadString(grepDevName);
                string rawSentence = newPosition.GetRaw();
                /*if ( rawSentence.EndsWith("\n") )
                {	System.Console.WriteLine("Stuff with Stuff"); 
                rawSentence.TrimEnd("\n");}*/
                screen.Output("Raw Gps String: " + rawSentence, 4, 5);
                screen.Output("Raw String Length: " + newPosition.GetLength(), 4, 5);
            }
            catch (System.Exception e)
            {
                screen.Output("GPS Error, make sure GPS device is plugged in.", 1, 1);
                screen.Output(e.Message, 1, 2); 		//e.ToString does more detailed
            }
            try
            { newPosition.Parse(); }
            catch (System.Exception e)
            {
                screen.Output(e.Message, 1, 2);
                //screen.Output(e.ToString(), 1, 2);
            }
            //For Globals
            time = newPosition.GetTime();
            lat_long = newPosition.GetLatitude() + ", " + newPosition.GetLongitude();

            //sends parsed gps sentence to screen for debuging purposes		 
            screen.Output("checksum= " + newPosition.IfChecksum() + ", time= " + time + ", Lat-Lon= " + newPosition.GetLatitude() +
             ", " + newPosition.GetLongitude() + ", speed= " + newPosition.GetSpeed() + ", bearing= " + newPosition.GetBearing() +
            ", valid fix= " + newPosition.IsValidFix(), 3, 4);

            //update position with the new position information
            Monitor.Enter(this);
            position = newPosition;
            Monitor.Exit(this);
        }

        private Bitmap GetImage()
        {
            //PROBLEM: for some reason, we are adding 4k to the normally 8k file, why?
            screen.Output("Getting Image...", 2, 3);
            String errorFrame = "no_frame.jpg";  //dan had this as orbiter.jpg, I don't think he understood what this was doing
            Camera camera = new Camera(camAddress, camUser, camPass, errorFrame, camType);

            try
            { camera.GetPicture(); }
            catch (System.Exception e)
            {
                screen.Output("Camera Error, make sure camera is working or plugged in.", 1, 1);
                screen.Output(e.Message, 1, 2); 		//e.ToString does more detailed
            }

            screen.Output("Stamping image with lat_long time: " + lat_long + "  " + time, 4, 5);
            camera.Stamp(lat_long, time);

            string imageName = camera.NamePicture();
            try { camera.SavePicture("./IMAGES/", imageName); }
            catch (System.Exception e)
            { screen.Output(e.Message, 1, 2); }
            screen.Output("Saving Image As: " + imageName, 3, 4);

            return (camera.ReturnPicture());
        }

        private void UploadImage(Bitmap image)
        {
            //Gps currentPosition;
            //create local copy of position variable
            Monitor.Enter(this);            
            Gps currentPosition = position;
            Monitor.Exit(this);

            screen.Output("Uploading To Server...", 2, 3);
            WebService webservice = new WebService(currentPosition, ActionTypes.GpsFixAcquired, image);

            screen.Output("Starting to send data to webservice...", 3, 4);
            try { webservice.Connect(); }
            catch (System.Exception e)
            {
                screen.Output("Error connecting to Internet.", 1, 1);
                screen.Output(e.Message, 1, 2);
            }
            screen.Output("Done sending data to webservice.", 3, 4);
        }

        private static int ArgParser(String[] args)
        {	//ugly argument parser, uses arguments.cs
            bool showHelp = false;	        //bad arg or need help, either way, offer help
            int vLevel = 2;					//defaults
            int svLevel = 1;				//again, default
            Arguments CommandLine = new Arguments(args);

            // Look for specific arguments values and display 
            // them if they exist (return null if they don't)		            	
            if (CommandLine["?"] != null || CommandLine["help"] != null)
            {
                Console.WriteLine("? value: " + CommandLine["?"]);
                showHelp = true;
            }
            //else
            //{ Console.WriteLine("? not defined."); }

            if (CommandLine["v"] != null)
            {	//v for verbosity
                Console.WriteLine("v value: " + CommandLine["v"]);
                try
                {
                    vLevel = Convert.ToInt32(CommandLine["v"]);
                    if ((vLevel < 0) || (vLevel > 4))
                    {
                        Console.WriteLine("Bad -v value, using defaults.");
                        vLevel = 2;
                    }
                }
                catch { Console.WriteLine("-v takes a number, using defaults."); }
            }
            verbosity = vLevel;

            if (CommandLine["s"] != null)
            {	//s for speech verbosity
                Console.WriteLine("s value: " + CommandLine["s"]);
                try
                {
                    svLevel = Convert.ToInt32(CommandLine["s"]);
                    if ((svLevel < 0) || (svLevel > 5))
                    {
                        Console.WriteLine("Bad -s value, using defaults.");
                        svLevel = 1;
                    }
                }
                catch { Console.WriteLine("-s takes a number, using defaults."); }
            }
            sVerbosity = svLevel;

            if (CommandLine["d"] != null)
            {	//d for device, can also be a file
                Console.WriteLine("Using file or device @ " + CommandLine["d"]);
                grepDevName = CommandLine["d"];
            }
            else
            { Console.WriteLine("Device name not defined, using default."); }

            if (CommandLine["a"] != null)
            {	//a for camera address
                Console.WriteLine("Camera Address set to " + CommandLine["a"]);
                camAddress = CommandLine["a"];
            }
            else
            { Console.WriteLine("Camera Address not defined, using default."); }

            if (CommandLine["u"] != null)
            {	//u for camera username
                Console.WriteLine("Camera Username set to " + CommandLine["u"]);
                camUser = CommandLine["u"];
            }
            else
            { Console.WriteLine("Camera Username not defined, using default."); }

            if (CommandLine["p"] != null)
            {	//p for camera password
                Console.WriteLine("Camera Password set to " + CommandLine["p"]);
                camPass = CommandLine["p"];
            }
            else
            { Console.WriteLine("Camera Password not defined, using default."); }

            if (CommandLine["t"] != null)
            {	//p for camera password
                if (CommandLine["t"] == "USB" || CommandLine["t"] == "NET")
                {
                    Console.WriteLine("Camera Type set to " + CommandLine["t"]);
                    camType = CommandLine["t"];
                }
                else
                { Console.WriteLine("Bad Camera Type, using default."); }
            }
            else
            { Console.WriteLine("Camera Type not defined, using default."); }


            if (showHelp == true)
            {   //PROBLEM: This prints out after using default messages, should probably print out before.
                Console.WriteLine("Available flags:\n  -v Verbosity\n  -s Speech Verbosity\n  -d Device Name or Path");
                Console.WriteLine("  -a Camera Address\n  -u Camera Username\n  -p Camera Password\n  -t Camera Type"); 
                return (-1);
            }
            return (0);
        }
    }
}
