//wrapper for asssetupdater.dll 
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using System.Drawing;

namespace GeoGraphicsLab.GpsTools
{
    class WebService
    {
        private byte[] cambytes;
        private SessionDetails session;
        GpsFix pos;
        DateTime now;
        ActionTypes actionType1;
        AssetUpdater ws;

        public WebService(Gps position, ActionTypes actionType, Bitmap pic)
        {	 //constructor      		
            try
            { 	//conversion time again
                cambytes = ConvertBit(pic);
                pic.Dispose();
            }
            catch
            { throw new System.Exception("Error converting Bitmap to Bytes."); }

            try
            {	//create AssetUpdater object	 
                ws = new AssetUpdater();
                //Console.WriteLine("AssetUpdater object created");		 
                //set timeout
                ws.Timeout = 5000;
                //Console.WriteLine("timeout set");
                //get time	 
                now = DateTime.Now;
                //Console.WriteLine("DateTime Set");	 

                //stumb test code, makes up assetId etc
                session = new SessionDetails();
                session.AssetID = 1;
                session.PersonnelID = 1;
                session.RouteID = 2;
                session.SessionRegistered = true;

                //code to make UpdateAsset see GpsFix, and not GPRMCSentence
                pos = new GpsFix();
                pos.Latitude = position.GetLatitude();
                pos.Longitude = position.GetLongitude();
                pos.Bearing = position.GetBearing();
                pos.Speed = position.GetSpeed();
            }
            catch
            { throw new System.Exception("WebService Constructor Error."); }
        }

        public void Connect()
        {	//send data to webservice
            try
            { ws.UpdateAsset(session, actionType1, pos, now, cambytes); }
            catch
            { throw new System.Exception("Error connecting to server."); }
        }

        private static byte[] ConvertBit(Bitmap pic)
        {   // Bitmap bytes have to be created via a direct memory copy of the bitmap
            MemoryStream ms = new MemoryStream();
            // Save to memory using the Jpeg format
            pic.Save(ms, ImageFormat.Jpeg);

            // read to end
            byte[] cambytes = ms.GetBuffer();
            pic.Dispose();
            ms.Close();

            return (cambytes);
        }

        /* //This is the old code, keeping this around, just in case anyone wants to see what colossal failure looks like
            //who knows, this could actually work with newer versions of mono
          public static byte[] ConvertBit ( Bitmap pic ) 
          {		//transform bitmap picture into byte stream
      
                PixelFormat pixelFormat = PixelFormat.Format24bppRgb;
      
                //Locks a Bitmap into system memory.
                BitmapData ptr = pic.LockBits(
                    new Rectangle(0, 0, pic.Width, pic.Height),
                    ImageLockMode.ReadOnly, 					//ImageLockMode.WriteOnly, 	
                    pixelFormat); //PixelFormat.Format24bppRgb); 
                int ptrLength = (ptr.Stride * ptr.Height) - 1;  		//was Int32, aparently this -1 will adjust it correctly, but still not fixing this
                //ptrLength = (pic.Width * pic.Height * 4) - 1;		//same as ptr.Stride * Height - 1
                byte[] cambytes = new byte[ptrLength];		
		 	
                //Copies data from a managed array to an unmanaged memory pointer, or from an unmanaged memory pointer to a managed array.
                Marshal.Copy(ptr.Scan0, cambytes, 0, ptrLength);
	       
               //System.Console.WriteLine( "ptrLength: "+ ptrLength);
               System.Console.WriteLine( "pic.Width: "+ pic.Width +" pic.Height: "+ pic.Height );
               System.Console.WriteLine( "ptr.Stride: "+ ptr.Stride +" ptr.Height: "+ ptr.Height );
               //System.Console.WriteLine( cambytes.ToString() );
	       
               System.Console.WriteLine( "pic info: "+ pic.Palette  +" " + pic.PixelFormat +" "+ pic.RawFormat );
	       	       
                //test code
                /*Byte [] testbytes = new Byte[1];
                    testbytes[0] = 1;   
	        	       
                Console.WriteLine("bitmap converted to bytestream");
                Console.WriteLine("cambyte length = " + cambytes.Length);
	        
               pic.UnlockBits(ptr);
	        
                Bitmap bmp2 =new Bitmap(pic.Width, pic.Height);
                BitmapData bmd2  = bmp2.LockBits(
                new Rectangle(0, 0, pic.Width, pic.Height), 
                ImageLockMode.WriteOnly, 
                pixelFormat); 
                Marshal.Copy(cambytes, 0, bmd2.Scan0, ptrLength);
                bmp2.UnlockBits(bmd2);
	      
	      
	        
                bmp2.Save("./images/whowhatwhere.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
	        
                return ( cambytes );	        
          }     */

    }
}
