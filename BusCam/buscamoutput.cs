using System;
using System.Diagnostics; 	//provides classes that allow you to interact with system processes, event logs, and performance counters.

namespace GeoGraphicsLab.GpsTools
{
    public class BusCamOutput
    {	//Output for Bus Cam (duh)
        private int verbosity = 4;			//verbosity level, 0 = lowest, 4 = highest
        private int sVerbosity = 1;		//speech verbosity level, 0 = lowest, 5 = highest

        public BusCamOutput(int verb, int sVerb)
        {	//Constructor
            verbosity = verb;
            sVerbosity = sVerb;

            //other possiblities--
            //pathname to say program
            //voice to use
        }

        public void Output(string oString, int type, int sType)
        { 	//function for simplied output for debugging/errors
            //Verbosity and type is 
            //0 = off, 1 == just error codes, 2 == normal output, 
            //3 == additional output, 4 == won't shut up
            // for voice 1 == default voice output, everything else +1

            if (type <= verbosity)
                //Good old console output
                System.Console.WriteLine(oString);

            if (sType <= sVerbosity)
            {
                Process speak = new Process();
                speak.StartInfo.UseShellExecute = false;
                speak.StartInfo.RedirectStandardOutput = true;
                speak.StartInfo.FileName = "say";
                speak.StartInfo.Arguments = " -v  Victoria \"" + oString + "\""; 		//note the quotes
                //Speak away
                speak.Start();
                speak.WaitForExit();
                //Error handling for our error handler
                if (speak.ExitCode != 0)
                    System.Console.WriteLine("say Process exit code: {0}", speak.ExitCode);
            }
        }
    }
}